package br.com.thiagonicacio.movies

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.matcher.RootMatchers.withDecorView
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import br.com.thiagonicacio.movies.presentation.login.LoginActivity
import br.com.thiagonicacio.movies.presentation.main.MainActivity
import br.com.thiagonicacio.movies.presentation.register.RegisterActivity
import org.hamcrest.Matchers.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class LoginActivityTest {

    @get:Rule
    val rule = ActivityTestRule(LoginActivity::class.java)

    @Test
    fun shouldShowErrorOnMandatoryField_WhenEmailFieldIsEmpty(){
        onView(withId(R.id.enterButton)).perform(click())
        onView(withId(R.id.email))
            .check(matches(isDisplayed()))
    }

    @Test
    fun shouldShowErrorOnMandatoryField_WhenPasswordFieldIsEmpty(){
        onView(withId(R.id.email))
            .perform(typeText("teste@teste.com"))
            .perform(closeSoftKeyboard())
        onView(withId(R.id.enterButton)).perform(click())
        onView(withId(R.id.password))
            .check(matches(isDisplayed()))
    }

    @Test
    fun shouldShowToast_WhenEmailOrPasswordIsInvalid(){
        onView(withId(R.id.email))
            .perform(typeText("asda"))
            .perform(closeSoftKeyboard())
        onView(withId(R.id.password))
            .perform(typeText("iublkjnasdfujo"))
            .perform(closeSoftKeyboard())
        onView(withId(R.id.enterButton)).perform(click())
        onView(withText(R.string.invalid_credentials))
            .inRoot(withDecorView(not(rule.activity.window.decorView))).
            check(matches(isDisplayed()))
    }

    @Test
    fun showMainActivity_WhenEmailAndPassWordIsCorrect(){
        onView(withId(R.id.email))
            .perform(typeText("t@t.com"))
            .perform(closeSoftKeyboard())
        onView(withId(R.id.password))
            .perform(typeText("t"))
            .perform(closeSoftKeyboard())
        Intents.init()
        onView(withId(R.id.enterButton)).perform(click())
        intended(hasComponent(MainActivity::class.java.name))
        Intents.release()
    }

    @Test
    fun showRegisterActivity_WhenRegisterClick(){
        Intents.init()
        onView(withId(R.id.register))
            .perform(click())
        intended(hasComponent(RegisterActivity::class.java.name))
        Intents.release()
    }
}