package br.com.thiagonicacio.movies.presentation.adapter

interface AdapterContract {
    fun replaceItems(items: List<*>)
}