package br.com.thiagonicacio.domain.repository

import br.com.thiagonicacio.domain.model.User
import io.reactivex.Single

interface UserRepository {
    suspend fun insertUser(email: String, name : String, password : String) : Single<Boolean>

    suspend fun getUser(email : String) : Single<User?>?

    suspend fun login(email: String, password: String) : Single<Boolean>
}