package br.com.thiagonicacio.data.storage.database

import androidx.room.*
import br.com.thiagonicacio.data.storage.database.entity.UserEntity

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.ABORT)
    suspend fun insertUser(user: UserEntity)

    @Query("SELECT * FROM user_table WHERE  email = :email")
    suspend fun getUser(email : String) : UserEntity?

    @Query("SELECT * FROM USER_TABLE WHERE email = :email AND password = :password")
    suspend fun login(email: String, password : String) : UserEntity?
}