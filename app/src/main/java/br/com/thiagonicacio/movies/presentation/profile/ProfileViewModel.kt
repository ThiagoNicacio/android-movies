package br.com.thiagonicacio.movies.presentation.profile

import android.app.Application
import androidx.lifecycle.MutableLiveData
import br.com.thiagonicacio.data.session.UserSessionManager
import br.com.thiagonicacio.domain.usecase.LogoutUseCase
import br.com.thiagonicacio.movies.presentation.BaseViewModel
import javax.inject.Inject

class ProfileViewModel @Inject constructor(
    private val router: ProfileRouter,
    private val logoutUseCase: LogoutUseCase,
    application: Application) : BaseViewModel(application) {

    val name = MutableLiveData<String>().apply { value = UserSessionManager.user.value?.name }
    val email = MutableLiveData<String>().apply { value = UserSessionManager.user.value?.email }

    fun onExitClicked(){
        logoutUseCase.execute()
        router.navigate(ProfileRouter.Route.LOGIN)
    }
}