package br.com.thiagonicacio.movies

import android.app.Application
import br.com.thiagonicacio.movies.di.application.ApplicationComponent
import br.com.thiagonicacio.movies.di.application.ApplicationModule
import br.com.thiagonicacio.movies.di.application.DaggerApplicationComponent

class BaseApplication : Application() {

    lateinit var component: ApplicationComponent

    override fun onCreate() {
        super.onCreate()

        inject()
    }

    fun inject() {
        component = DaggerApplicationComponent.builder().applicationModule(
            ApplicationModule(this)
        ).build()
        component.inject(this )
    }
}