package br.com.thiagonicacio.domain.usecase

import br.com.thiagonicacio.domain.RegisterResult
import br.com.thiagonicacio.domain.repository.UserRepository
import io.reactivex.Observable
import javax.inject.Inject

class RegisterUserCase @Inject constructor(
    private val userRepository: UserRepository
) {

    suspend fun execute(email : String, name : String, password: String) : Observable<RegisterResult> {
        return userRepository.insertUser(email, name, password)
            .toObservable()
            .map {
                RegisterResult.Success(it) as RegisterResult
            }
            .onErrorReturn { RegisterResult.Failure(it) }
            .startWith( RegisterResult.Loading )
    }
}