package br.com.thiagonicacio.domain.usecase

import br.com.thiagonicacio.domain.FavoriteResult
import br.com.thiagonicacio.domain.model.Movie
import br.com.thiagonicacio.domain.repository.FavoriteRepository
import io.reactivex.Observable
import javax.inject.Inject

class FavoriteUseCase @Inject constructor(
    private val favoriteRepository: FavoriteRepository
) {

    suspend fun execute(movie : Movie) : Observable<FavoriteResult> {
        return favoriteRepository.insertFavorite(movie)
            .toObservable()
            .map {
                FavoriteResult.Success(it) as FavoriteResult
            }
            .onErrorReturn { FavoriteResult.Failure(it) }
            .startWith( FavoriteResult.Loading )
    }
}