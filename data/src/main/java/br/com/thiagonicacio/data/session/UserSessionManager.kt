package br.com.thiagonicacio.data.session

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.thiagonicacio.domain.model.User

object UserSessionManager {
    private val _user = MutableLiveData<User?>().apply { value = null }
    val user : LiveData<User?>
        get() = _user

    fun updateUser(user: User?){
        _user.postValue(user)
    }

    fun clear() {
        _user.postValue(null)
    }
}