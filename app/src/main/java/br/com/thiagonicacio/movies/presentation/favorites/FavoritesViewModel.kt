package br.com.thiagonicacio.movies.presentation.favorites

import android.app.Application
import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import br.com.thiagonicacio.domain.FavoriteResult
import br.com.thiagonicacio.domain.GetAllFavoriteResult
import br.com.thiagonicacio.domain.model.Movie
import br.com.thiagonicacio.domain.usecase.DisfavorUseCase
import br.com.thiagonicacio.domain.usecase.GetAllFavoritesUseCase
import br.com.thiagonicacio.movies.presentation.BaseViewModel
import br.com.thiagonicacio.movies.presentation.util.ExtraUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.launch
import javax.inject.Inject

class FavoritesViewModel @Inject constructor(
    private val router: FavoriteRouter,
    private val getAllFavoritesUseCase: GetAllFavoritesUseCase,
    private val disfavorUseCase: DisfavorUseCase,
    application: Application): BaseViewModel(application) {

    private val _movies = MutableLiveData<List<Movie>?>().apply { value = null }
    val movies : LiveData<List<Movie>?> = _movies
    val nonBlockingLoading = MutableLiveData<Boolean>().apply { value = false }
    val noMovies = MutableLiveData<Boolean>().apply { value = false }
    private val _failureMovies = MutableLiveData<Boolean>().apply { value = false }
    val failureMovies : LiveData<Boolean> = _failureMovies
    private val _failureFavoriteMovie = MutableLiveData<Boolean>().apply { value = false }
    val failureFavoriteMovie : LiveData<Boolean> = _failureFavoriteMovie

    init {
        getListMovies()
    }

    override fun onClickItem(item: Any) {
        if (item is Movie){
            router.navigate(FavoriteRouter.Route.MOVIE_DETAILS, Bundle().apply { putSerializable(
                ExtraUtil.EXTRA_MOVIE, item) })
        }
    }

    private fun getListMovies(){
        viewModelScope.launch {
            getAllFavoritesUseCase.execute()
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe {handleGetMovies(it)}
                ?.addTo(disposables)
        }
    }

    fun onResume(){
        getListMovies()
    }

    fun onDisfavorClicked(movie: Movie){
        viewModelScope.launch {
            disfavorUseCase.execute(movie)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { handleFavoriteMovie(it) }
                .addTo(disposables)
        }
    }

    private fun handleFavoriteMovie(result : FavoriteResult){
        when(result){
            is FavoriteResult.Success ->{
                hideDialog()
                getListMovies()
            }
            is FavoriteResult.Failure ->{
                hideDialog()
                _failureFavoriteMovie.postValue(true)
            }
            is FavoriteResult.Loading -> {
                showDialog()
            }
        }
    }

    private fun handleGetMovies(result : GetAllFavoriteResult){
        nonBlockingLoading.postValue(result == GetAllFavoriteResult.Loading)
        when(result){
            is GetAllFavoriteResult.Success ->{
                noMovies.postValue(result.movies.isNullOrEmpty())
                _movies.postValue(result.movies)
            }
            is GetAllFavoriteResult.Failure ->{
                _failureMovies.postValue(true)
            }
        }
    }
}