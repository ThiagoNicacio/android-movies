package br.com.thiagonicacio.movies.presentation.register

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import br.com.thiagonicacio.domain.RegisterResult
import br.com.thiagonicacio.domain.usecase.RegisterUserCase
import br.com.thiagonicacio.movies.presentation.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.launch
import javax.inject.Inject

class RegisterViewModel @Inject constructor(
    private val router: RegisterRouter,
    private val registerUserCase: RegisterUserCase,
    application: Application ) : BaseViewModel(application){

    val email = MutableLiveData<String>().apply { value = "" }
    val name = MutableLiveData<String>().apply { value = "" }
    val password = MutableLiveData<String>().apply { value = "" }
    val confirmPassword = MutableLiveData<String>().apply { value = "" }

    private val _emailError = MutableLiveData<Boolean>().apply { value = false }
    val emailError : LiveData<Boolean> = _emailError

    private val _nameError = MutableLiveData<Boolean>().apply { value = false }
    val nameError : LiveData<Boolean> = _nameError

    private val _passwordError = MutableLiveData<Boolean>().apply { value = false }
    val passwordError : LiveData<Boolean> = _passwordError

    private val _confirmPasswordError = MutableLiveData<Boolean>().apply { value = false }
    val confirmPasswordError : LiveData<Boolean> = _confirmPasswordError

    private val _incorrectPasswordConfirmation = MutableLiveData<Boolean>().apply { value = false }
    val incorrectPasswordConfirmation : LiveData<Boolean> = _incorrectPasswordConfirmation

    private val _existRegister = MutableLiveData<Boolean>().apply { value = false }
    val existRegister : LiveData<Boolean> = _existRegister

    private val _successRegister = MutableLiveData<Boolean>().apply { value = false }
    val successRegister : LiveData<Boolean> = _successRegister

    private val _failureRegister = MutableLiveData<Boolean>().apply { value = false }
    val failureRegister : LiveData<Boolean> = _failureRegister

    fun onRegisterClicked(){
        _emailError.value = email.value.isNullOrBlank()
        _nameError.value = name.value.isNullOrBlank()
        _passwordError.value = password.value.isNullOrBlank()
        _confirmPasswordError.value = confirmPassword.value.isNullOrBlank()

        if(_emailError.value!! ||
           _nameError.value!! ||
           _passwordError.value!! ||
           _confirmPasswordError.value!!)
            return

        _incorrectPasswordConfirmation.value = password.value != confirmPassword.value
        if (incorrectPasswordConfirmation.value!!)
            return

        viewModelScope.launch {
            registerUserCase.execute(email.value!!, name.value!!, password.value!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { handleRegister(it) }
                .addTo(disposables)
        }
    }

    private fun handleRegister(result : RegisterResult){
        when(result){
            is RegisterResult.Success ->{
                hideDialog()
                if (result.boolean){
                    _successRegister.value = true
                    router.goBack()
                }
                else
                    _existRegister.value = true
            }
            is RegisterResult.Failure ->{
                hideDialog()
                _failureRegister.postValue(true)
            }
            is RegisterResult.Loading -> {
                showDialog()
            }
        }
    }

    fun onBackClicked(){
        router.goBack()
    }
}