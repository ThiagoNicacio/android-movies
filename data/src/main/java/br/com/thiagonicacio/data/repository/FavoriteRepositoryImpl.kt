package br.com.thiagonicacio.data.repository

import br.com.thiagonicacio.data.storage.database.AppDatabase
import br.com.thiagonicacio.data.mapper.MovieMapper
import br.com.thiagonicacio.data.session.UserSessionManager
import br.com.thiagonicacio.domain.model.Movie
import br.com.thiagonicacio.domain.repository.FavoriteRepository
import io.reactivex.Single
import javax.inject.Inject

class FavoriteRepositoryImpl @Inject constructor(
    private val movieMapper: MovieMapper,
    private val appDatabase: AppDatabase): FavoriteRepository{

    override suspend fun insertFavorite(movie: Movie): Single<Boolean> {
        return try {
            appDatabase.movieDao.insertMovie(movieMapper.map(movie, UserSessionManager.user.value?.email!!))
            Single.just(true)
        }catch (e : Exception){
            Single.just(false)
        }
    }

    override suspend  fun getAllFavorites(): Single<List<Movie>?> {
        val lst = appDatabase.movieDao.getMovies(UserSessionManager.user.value?.email!!) ?: return Single.just(arrayListOf())
        return Single.just(movieMapper.map2(lst))
    }

    override suspend fun getFavorite(id: Int): Single<Movie?>? {
        val mv = appDatabase.movieDao.getMovie(id, UserSessionManager.user.value?.email!!) ?: return null
        return Single.just(movieMapper.map(mv))
    }

    override suspend fun deleteFavorite(movie: Movie): Single<Boolean> {
        return try {
            appDatabase.movieDao.deleteMovie(movieMapper.map(movie, UserSessionManager.user.value?.email!!))
            Single.just(true)
        }
        catch (e : Exception){
            Single.just(false)
        }
    }
}