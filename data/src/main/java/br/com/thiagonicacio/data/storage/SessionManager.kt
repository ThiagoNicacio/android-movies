package br.com.thiagonicacio.data.storage

import android.app.Application
import javax.inject.Inject

class SessionManager @Inject constructor(application: Application): SharedPreferencesHelper(application) {

    companion object {
        const val KEEP_LOGGED = "keep_logged"
        const val EMAIL = "email"
    }

    fun saveRemember(keepLogged: Boolean = false){
        putString(KEEP_LOGGED, keepLogged.toString())
    }

    fun getRemember() : Boolean{
        return if (getString(KEEP_LOGGED) != null)
            getString(KEEP_LOGGED)!!.toBoolean()
        else false
    }

    fun saveEmail(email : String){
        putString(EMAIL, email)
    }

    fun getEmail() : String{
       val em = getString(EMAIL)
        if (em !== null) return em
        return ""
    }

    fun logout() = erase()
}