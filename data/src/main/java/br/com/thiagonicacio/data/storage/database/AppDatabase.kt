package br.com.thiagonicacio.data.storage.database

import androidx.room.Database
import androidx.room.RoomDatabase
import br.com.thiagonicacio.data.storage.database.entity.MovieEntity
import br.com.thiagonicacio.data.storage.database.entity.UserEntity

@Database(entities = [MovieEntity::class, UserEntity::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase(){

    abstract val movieDao: MovieDao
    abstract val userDao : UserDao

    companion object {
        const val DB_NAME = "movie_database.db"
    }
}