package br.com.thiagonicacio.domain.usecase

import br.com.thiagonicacio.domain.GetUserResult
import br.com.thiagonicacio.domain.repository.UserRepository
import io.reactivex.Observable
import javax.inject.Inject

class GetUserUseCase @Inject constructor(
    private val userRepository: UserRepository
) {

    suspend fun execute(email : String) : Observable<GetUserResult>? {
        return userRepository.getUser(email)
            ?.toObservable()
            ?.map {
                GetUserResult.Success(it) as GetUserResult
            }
            ?.onErrorReturn { GetUserResult.Failure(it) }
            ?.startWith( GetUserResult.Loading )
    }
}