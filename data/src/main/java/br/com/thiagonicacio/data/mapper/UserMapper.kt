package br.com.thiagonicacio.data.mapper

import br.com.thiagonicacio.data.storage.database.entity.UserEntity
import br.com.thiagonicacio.domain.model.User
import javax.inject.Inject

class UserMapper @Inject constructor(){
    fun map(response : UserEntity) : User{
        return User(
            email = response.email,
            name = response.name
        )
    }
}