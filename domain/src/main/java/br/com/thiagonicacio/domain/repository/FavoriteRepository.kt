package br.com.thiagonicacio.domain.repository

import androidx.lifecycle.LiveData
import br.com.thiagonicacio.domain.model.Movie
import io.reactivex.Single

interface FavoriteRepository{
    suspend fun insertFavorite(movie: Movie) : Single<Boolean>

    suspend fun getAllFavorites() : Single<List<Movie>?>

    suspend fun getFavorite(id : Int) : Single<Movie?>?

    suspend fun deleteFavorite(movie: Movie) : Single<Boolean>
}