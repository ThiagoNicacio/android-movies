package br.com.thiagonicacio.movies.presentation.moviedetails

import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import br.com.thiagonicacio.movies.R
import br.com.thiagonicacio.movies.R.layout
import br.com.thiagonicacio.movies.databinding.ActivityMovieDetailsBinding
import br.com.thiagonicacio.movies.presentation.BaseActivity
import br.com.thiagonicacio.movies.presentation.util.ExtraUtil
import javax.inject.Inject

class MovieDetailsActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: MovieDetailsViewModel
    override fun getBaseViewModel() = viewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding : ActivityMovieDetailsBinding = DataBindingUtil.setContentView(this, layout.activity_movie_details)

        screenComponent.inject(this)

        binding.let {
            it.lifecycleOwner = this
            it.viewModel = viewModel
        }

        viewModel.let {
            it.failureDisfavorMovie.observe(this,
                Observer {bool->
                    if(bool) Toast.makeText(this,getString(R.string.failed_to_disfavor_movie), Toast.LENGTH_LONG).show()
                }
            )

            it.failureFavoriteMovie.observe(this,
                Observer {bool->
                    if(bool) Toast.makeText(this,getString(R.string.failed_to_favorite_movie), Toast.LENGTH_LONG).show()
                }
            )
        }

        intent.getSerializableExtra(ExtraUtil.EXTRA_MOVIE)?.let { viewModel.setMovie(it) }
    }
}