package br.com.thiagonicacio.data.storage.database

import androidx.room.*
import br.com.thiagonicacio.data.storage.database.entity.MovieEntity

@Dao
interface MovieDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertMovie(movie: MovieEntity)

    @Query("SELECT * FROM movie_table WHERE user = :user")
    suspend fun getMovies(user : String): List<MovieEntity>?

    @Query("SELECT * FROM movie_table WHERE id = :id AND user = :user")
    suspend fun getMovie(id : Int, user : String) : MovieEntity?

    @Delete
    suspend fun deleteMovie(movie: MovieEntity)
}