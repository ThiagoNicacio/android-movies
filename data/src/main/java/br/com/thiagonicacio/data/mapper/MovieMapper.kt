package br.com.thiagonicacio.data.mapper

import br.com.thiagonicacio.data.storage.database.entity.MovieEntity
import br.com.thiagonicacio.data.response.MovieResponse
import br.com.thiagonicacio.domain.model.Movie
import javax.inject.Inject

class MovieMapper @Inject constructor(){
    fun map(response : List<MovieResponse>?) : List<Movie>?{
        if (response == null)
            return null
        return response.map { map(it) }
    }

    fun map(response : MovieResponse) : Movie{
        return Movie(
            id = response.id,
            title = response.title,
            subTitle = response.sub_title,
            duration = response.duration,
            synopsis = response.synopsis,
            thumb = response.thumb
        )
    }

    fun map(response : Movie, user : String) : MovieEntity{
        return MovieEntity(
            id = response.id,
            title = response.title,
            sub_title = response.subTitle,
            duration = response.duration,
            synopsis = response.synopsis,
            thumb = response.thumb,
            user = user
        )
    }

    fun map2(response : List<MovieEntity>?) : List<Movie>{
        if (response == null)
            return arrayListOf()
        return response.map { map(it) }
    }

    fun map(response : MovieEntity) : Movie{
        return Movie(
            id = response.id,
            title = response.title,
            subTitle = response.sub_title,
            duration = response.duration,
            synopsis = response.synopsis,
            thumb = response.thumb
        )
    }
}