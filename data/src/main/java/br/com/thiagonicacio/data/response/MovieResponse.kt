package br.com.thiagonicacio.data.response

import androidx.room.Entity
import androidx.room.PrimaryKey

data class MovieResponse(
    val id : Int,
    val title: String,
    val sub_title: String,
    val duration : String,
    val synopsis : String,
    val thumb : String
)