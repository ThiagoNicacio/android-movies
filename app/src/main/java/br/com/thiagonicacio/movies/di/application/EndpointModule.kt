package br.com.thiagonicacio.movies.di.application

import br.com.thiagonicacio.data.api.MovieEndpoint
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class EndpointModule {

    @Provides
    @Singleton
    fun provideMoviesEndpoint(retrofit: Retrofit) : MovieEndpoint{
        return retrofit.create(MovieEndpoint::class.java)
    }
}