package br.com.thiagonicacio.data.repository

import br.com.thiagonicacio.data.session.UserSessionManager
import br.com.thiagonicacio.data.storage.SessionManager
import br.com.thiagonicacio.domain.repository.AuthRepository
import io.reactivex.Single
import javax.inject.Inject

class AuthRepositoryImpl @Inject constructor(private val session : SessionManager) : AuthRepository{

    override fun setRemember(boolean: Boolean, email : String) {
        session.saveRemember(boolean)
        if (boolean) session.saveEmail(email)
    }

    override fun getRemember(): Boolean {
        return session.getRemember()
    }

    override fun getEmail(): String? {
        return session.getEmail()
    }

    override fun logout() {
        session.logout()
        UserSessionManager.clear()
    }
}