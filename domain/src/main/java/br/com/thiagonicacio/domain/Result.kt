package br.com.thiagonicacio.domain

import br.com.thiagonicacio.domain.model.Movie
import br.com.thiagonicacio.domain.model.User

/*
    Essa classe serve apenas para criar os results dos casos de uso.
    Pois em alguns casos de uso o result pode ser o mesmo
 */

sealed class MoviesResult {
    object Loading : MoviesResult()
    data class Success(val movies: List<Movie>) : MoviesResult()
    data class Failure(val throwable: Throwable) : MoviesResult()
}

sealed class FavoriteResult {
    object Loading : FavoriteResult()
    data class Success(val boolean: Boolean) : FavoriteResult()
    data class Failure(val throwable: Throwable) : FavoriteResult()
}

sealed class RememberResult {
    object Loading : RememberResult()
    data class Success(val email: String?) : RememberResult()
    data class Failure(val throwable: Throwable) : RememberResult()
}

sealed class GetFavoriteResult {
    object Loading : GetFavoriteResult()
    data class Success(val movie: Movie?) : GetFavoriteResult()
    data class Failure(val throwable: Throwable) : GetFavoriteResult()
}

sealed class GetAllFavoriteResult {
    object Loading : GetAllFavoriteResult()
    data class Success(val movies: List<Movie>?) : GetAllFavoriteResult()
    data class Failure(val throwable: Throwable) : GetAllFavoriteResult()
}

sealed class RegisterResult {
    object Loading : RegisterResult()
    data class Success(val boolean: Boolean) : RegisterResult()
    data class Failure(val throwable: Throwable) : RegisterResult()
}

sealed class LoginResult {
    object Loading : LoginResult()
    data class Success(val boolean: Boolean) : LoginResult()
    data class Failure(val throwable: Throwable) : LoginResult()
}

sealed class GetUserResult {
    object Loading : GetUserResult()
    data class Success(val user: User?) : GetUserResult()
    data class Failure(val throwable: Throwable) : GetUserResult()
}