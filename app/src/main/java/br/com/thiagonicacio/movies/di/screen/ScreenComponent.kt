package br.com.thiagonicacio.movies.di.screen

import br.com.thiagonicacio.movies.di.scope.PerScreen
import br.com.thiagonicacio.movies.presentation.favorites.FavoritesFragment
import br.com.thiagonicacio.movies.presentation.login.LoginActivity
import br.com.thiagonicacio.movies.presentation.main.MainActivity
import br.com.thiagonicacio.movies.presentation.moviedetails.MovieDetailsActivity
import br.com.thiagonicacio.movies.presentation.movies.MoviesFragment
import br.com.thiagonicacio.movies.presentation.profile.ProfileFragment
import br.com.thiagonicacio.movies.presentation.register.RegisterActivity
import dagger.Subcomponent

@PerScreen
@Subcomponent(modules = [ScreenModule::class])
interface ScreenComponent {
    fun inject(mainActivity: MainActivity)
    fun inject(moviesFragment: MoviesFragment)
    fun inject(favoritesFragment: FavoritesFragment)
    fun inject(movieDetailsActivity: MovieDetailsActivity)
    fun inject(loginActivity: LoginActivity)
    fun inject(registerActivity: RegisterActivity)
    fun inject(profileFragment: ProfileFragment)
}