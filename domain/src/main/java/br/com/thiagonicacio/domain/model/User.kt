package br.com.thiagonicacio.domain.model

data class User(
    val email : String,
    val name : String
)