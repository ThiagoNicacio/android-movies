package br.com.thiagonicacio.movies.presentation.moviedetails

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import br.com.thiagonicacio.domain.FavoriteResult
import br.com.thiagonicacio.domain.GetFavoriteResult
import br.com.thiagonicacio.domain.model.Movie
import br.com.thiagonicacio.domain.usecase.DisfavorUseCase
import br.com.thiagonicacio.domain.usecase.FavoriteUseCase
import br.com.thiagonicacio.domain.usecase.GetFavoriteUseCase
import br.com.thiagonicacio.movies.presentation.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.launch
import java.io.Serializable
import javax.inject.Inject

class MovieDetailsViewModel @Inject constructor(
    private val router: MovieDetailsRouter,
    private val favoriteUseCase : FavoriteUseCase,
    private val getFavoriteUseCase: GetFavoriteUseCase,
    private val disfavorUseCase: DisfavorUseCase,
    application: Application
) : BaseViewModel(application){

    val movie = MutableLiveData<Movie>().apply { value = null }
    val favorite = MutableLiveData<Boolean>().apply { value = false }
    private val _failureFavoriteMovie = MutableLiveData<Boolean>().apply { value = false }
    val failureFavoriteMovie : LiveData<Boolean> = _failureFavoriteMovie
    private val _failureDisfavorMovie = MutableLiveData<Boolean>().apply { value = false }
    val failureDisfavorMovie : LiveData<Boolean> = _failureDisfavorMovie

    fun setMovie(movie : Serializable){
        this.movie.postValue(movie as Movie)
        viewModelScope.launch {
            getFavoriteUseCase.execute(movie.id)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe {handleGetFavorite(it)}
                ?.addTo(disposables)
        }
    }

    fun goBack(){
        router.goBack()
    }

    fun onFavoriteClicked(){
        viewModelScope.launch {
            if (favorite.value!!){
                disfavorUseCase.execute(movie.value!!)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { handleFavoriteMovie(it) }
                    .addTo(disposables)
            }
            else {
                favoriteUseCase.execute(movie.value!!)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { handleFavoriteMovie(it) }
                    .addTo(disposables)
            }
        }
    }

    private fun handleFavoriteMovie(result : FavoriteResult){
        when(result){
            is FavoriteResult.Success ->{
               favorite.value = !favorite.value!!
            }
            is FavoriteResult.Failure ->{
                if (favorite.value!!)
                    _failureDisfavorMovie.postValue(true)
                else
                    _failureFavoriteMovie.postValue(true)
            }
        }
    }

    private fun handleGetFavorite(result : GetFavoriteResult){
        when(result){
            is GetFavoriteResult.Success ->{
                if (result.movie != null && movie.value!!.id == result.movie?.id)
                    favorite.value = !favorite.value!!
            }
        }
    }
}