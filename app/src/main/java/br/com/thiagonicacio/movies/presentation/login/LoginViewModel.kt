package br.com.thiagonicacio.movies.presentation.login

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import br.com.thiagonicacio.data.session.UserSessionManager
import br.com.thiagonicacio.domain.GetUserResult
import br.com.thiagonicacio.domain.LoginResult
import br.com.thiagonicacio.domain.RegisterResult
import br.com.thiagonicacio.domain.RememberResult
import br.com.thiagonicacio.domain.usecase.GetUserUseCase
import br.com.thiagonicacio.domain.usecase.LoginUseCase
import br.com.thiagonicacio.domain.usecase.RememberUseCase
import br.com.thiagonicacio.movies.presentation.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.launch
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    private val router: LoginRouter,
    private val rememberUseCase: RememberUseCase,
    private val loginUseCase: LoginUseCase,
    private val getUserUseCase: GetUserUseCase,
    application: Application
) : BaseViewModel(application){

    init {
        viewModelScope.launch {
            rememberUseCase.execute()?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe {handleUser(it)}
                ?.addTo(disposables)
        }
    }

    val email = MutableLiveData<String>().apply { value = "" }
    val password = MutableLiveData<String>().apply { value = "" }
    val remember = MutableLiveData<Boolean>().apply { value = false }

    private val _emailError = MutableLiveData<Boolean>().apply { value = false }
    val emailError : LiveData<Boolean> = _emailError

    private val _passwordError = MutableLiveData<Boolean>().apply { value = false }
    val passwordError : LiveData<Boolean> = _passwordError

    private val _invalidCredentials = MutableLiveData<Boolean>().apply { value = false }
    val invalidCredentials : LiveData<Boolean> = _invalidCredentials

    private val _failureLogin = MutableLiveData<Boolean>().apply { value = false }
    val failureLogin : LiveData<Boolean> = _failureLogin

    private val _failureGetUser = MutableLiveData<Boolean>().apply { value = false }
    val failureGetUser : LiveData<Boolean> = _failureGetUser

    fun onEnterClicked(){
        _emailError.value = email.value.isNullOrBlank()
        _passwordError.value = password.value.isNullOrBlank()

        if(_emailError.value!! ||
            _passwordError.value!!)
            return

        viewModelScope.launch {
            loginUseCase.execute(email.value!!, password.value!!, remember.value!!)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe { handleLogin(it) }
                ?.addTo(disposables)
        }
    }

    private fun handleLogin(result : LoginResult){
        when(result){
            is LoginResult.Success ->{
                if (result.boolean){
                    getUser(email.value!!)
                }
                else{
                    hideDialog()
                    _invalidCredentials.postValue(true)
                }
            }
            is LoginResult.Failure ->{
                hideDialog()
                _failureLogin.postValue(true)
            }
            is LoginResult.Loading -> {
                showDialog()
            }
        }
    }

    fun onSignUpClicked(){
        router.navigate(LoginRouter.Route.REGISTER)
    }

    private fun getUser(email : String){
        viewModelScope.launch {
            getUserUseCase.execute(email)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe { handleUser(it) }
                ?.addTo(disposables)
        }
    }

    private fun handleUser(result : GetUserResult){
        when(result){
            is GetUserResult.Success ->{
                hideDialog()
                UserSessionManager.updateUser(result.user)
                router.navigate(LoginRouter.Route.MAIN)
            }
            is GetUserResult.Failure ->{
                hideDialog()
                _failureGetUser.postValue(true)
            }
            is GetUserResult.Loading -> {
                showDialog()
            }
        }
    }
}