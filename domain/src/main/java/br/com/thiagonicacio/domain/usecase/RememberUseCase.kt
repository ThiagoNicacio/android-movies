package br.com.thiagonicacio.domain.usecase

import br.com.thiagonicacio.domain.GetUserResult
import br.com.thiagonicacio.domain.repository.AuthRepository
import io.reactivex.Observable
import javax.inject.Inject

class RememberUseCase @Inject constructor(private val authRepository: AuthRepository, private val getUserUseCase: GetUserUseCase){
    suspend fun execute() : Observable<GetUserResult>? {

        if(authRepository.getRemember() && authRepository.getEmail() != null){
            return getUserUseCase.execute(authRepository.getEmail()!!)
        }
        else{
            authRepository.logout()
        }
        return null
    }
}