package br.com.thiagonicacio.movies.presentation.favorites

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridLayout
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.thiagonicacio.movies.R
import br.com.thiagonicacio.movies.databinding.FavoritesFragmentBinding
import br.com.thiagonicacio.movies.presentation.BaseFragment
import br.com.thiagonicacio.movies.presentation.adapter.FavoriteAdapter
import javax.inject.Inject

class FavoritesFragment : BaseFragment() {

    companion object {
        fun newInstance() = FavoritesFragment()
    }

    @Inject
    lateinit var viewModel: FavoritesViewModel

    override fun getBaseViewModel() = viewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding : FavoritesFragmentBinding = FavoritesFragmentBinding.inflate(inflater,
            container, false)
        screenComponent.inject(this)

        binding.let {
            it.lifecycleOwner = requireActivity()
            it.viewModel = viewModel
            it.recyclerFavorites.layoutManager = getLayoutManager()
        }

        viewModel.movies.observe(viewLifecycleOwner,
            Observer {
                it?.let{movies ->
                    val adapter = FavoriteAdapter(movies)
                    adapter.onItemClickedListener = { viewModel.onClickItem(it) }
                    adapter.onDisfavorClickedListener = { viewModel.onDisfavorClicked(it) }
                    binding.recyclerFavorites.adapter = adapter
                }
            }
        )
        
        viewModel.let { 
            it.failureMovies.observe(viewLifecycleOwner, 
                Observer {bool-> 
                  if(bool) Toast.makeText(requireContext(),getString(R.string.Failed_get_favorites_list), Toast.LENGTH_LONG).show()
                }
            )

            it.failureFavoriteMovie.observe(viewLifecycleOwner,
                Observer {bool->
                    if(bool) Toast.makeText(requireContext(),getString(R.string.failed_to_disfavor_movie), Toast.LENGTH_LONG).show()
                }
            )
        }

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        viewModel.onResume()
    }

    fun getLayoutManager() : RecyclerView.LayoutManager{
        return if (isTablet()){
            GridLayoutManager(requireContext(), 3, GridLayoutManager.VERTICAL, false)
        }
        else{
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        }
    }


    fun isTablet(): Boolean {
        return resources.configuration.smallestScreenWidthDp >= 600
    }
}