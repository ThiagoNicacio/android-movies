package br.com.thiagonicacio.domain.usecase

import br.com.thiagonicacio.domain.LoginResult
import br.com.thiagonicacio.domain.repository.AuthRepository
import br.com.thiagonicacio.domain.repository.UserRepository
import io.reactivex.Observable
import javax.inject.Inject

class LoginUseCase @Inject constructor(
    private val userRepository: UserRepository,
    private val authRepository: AuthRepository
) {

    suspend fun execute(email : String, password: String, remember : Boolean) : Observable<LoginResult>? {
        return userRepository.login(email, password)
            .toObservable()
            .map {
                authRepository.setRemember(remember, email)
                LoginResult.Success(it) as LoginResult
            }
            .onErrorReturn { LoginResult.Failure(it) }
            .startWith( LoginResult.Loading )
    }
}