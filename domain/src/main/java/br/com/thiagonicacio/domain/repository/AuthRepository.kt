package br.com.thiagonicacio.domain.repository

import io.reactivex.Single

interface AuthRepository{
    fun setRemember(boolean: Boolean, email : String)

    fun getRemember() : Boolean

    fun getEmail() : String?

    fun logout()
}