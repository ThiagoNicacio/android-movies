package br.com.thiagonicacio.domain.usecase

import br.com.thiagonicacio.domain.MoviesResult
import br.com.thiagonicacio.domain.repository.MovieRepository
import io.reactivex.Observable
import javax.inject.Inject

class GetMovieUseCase @Inject constructor(
    private val movieRepository: MovieRepository) {

    fun execute() : Observable<MoviesResult>{
        return movieRepository.getMovies()
            .toObservable()
            .map {
                MoviesResult.Success(it) as MoviesResult
            }
            .onErrorReturn { MoviesResult.Failure(it) }
            .startWith( MoviesResult.Loading )
    }
}