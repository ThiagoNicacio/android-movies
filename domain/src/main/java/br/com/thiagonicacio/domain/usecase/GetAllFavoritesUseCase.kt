package br.com.thiagonicacio.domain.usecase

import br.com.thiagonicacio.domain.GetAllFavoriteResult
import br.com.thiagonicacio.domain.repository.FavoriteRepository
import io.reactivex.Observable
import javax.inject.Inject

class GetAllFavoritesUseCase @Inject constructor(
    private val favoriteRepository: FavoriteRepository
) {

    suspend fun execute() : Observable<GetAllFavoriteResult> {
        return favoriteRepository.getAllFavorites()
            .toObservable()
            .map {
                GetAllFavoriteResult.Success(it) as GetAllFavoriteResult
            }
            .onErrorReturn { GetAllFavoriteResult.Failure(it) }
            .startWith( GetAllFavoriteResult.Loading )
    }
}