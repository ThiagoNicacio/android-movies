package br.com.thiagonicacio.movies.presentation.movies

import android.app.Application
import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.thiagonicacio.domain.MoviesResult
import br.com.thiagonicacio.domain.model.Movie
import br.com.thiagonicacio.domain.usecase.GetMovieUseCase
import br.com.thiagonicacio.movies.presentation.BaseViewModel
import br.com.thiagonicacio.movies.presentation.util.ExtraUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MoviesViewModel @Inject constructor(
    private val getMovieUseCase: GetMovieUseCase,
    private val router: MoviesRouter,
    application : Application): BaseViewModel(application) {

    val nonBlockingLoading = MutableLiveData<Boolean>().apply { value = false }
    private val _movies = MutableLiveData<List<Movie>?>().apply { value = null }
    val movies : LiveData<List<Movie>?> = _movies
    val noMovies = MutableLiveData<Boolean>().apply { value = false }
    private val _failureMovies = MutableLiveData<Boolean>().apply { value = false }
    val failureMovies : LiveData<Boolean> = _failureMovies

    init {
        getMovies()
    }

    override fun onClickItem(item: Any) {
        if (item is Movie){
            router.navigate(MoviesRouter.Route.MOVIE_DETAILS, Bundle().apply { putSerializable(ExtraUtil.EXTRA_MOVIE, item) })
        }
    }

    private fun getMovies(){
        getMovieUseCase.execute()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {handleGetMovies(it)}
            .addTo(disposables)
    }

    private fun handleGetMovies(result : MoviesResult){
        nonBlockingLoading.postValue(result == MoviesResult.Loading)
        when(result){
            is MoviesResult.Success ->{
                noMovies.postValue(result.movies.isNullOrEmpty())
                _movies.postValue(result.movies)
            }
            is MoviesResult.Failure ->{
                _failureMovies.postValue(true)
            }
        }
    }
}