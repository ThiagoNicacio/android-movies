package br.com.thiagonicacio.movies.presentation.register

import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import br.com.thiagonicacio.movies.R
import br.com.thiagonicacio.movies.databinding.ActivityRegisterBinding
import br.com.thiagonicacio.movies.presentation.BaseActivity
import javax.inject.Inject

class RegisterActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: RegisterViewModel

    override fun getBaseViewModel() = viewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding : ActivityRegisterBinding = DataBindingUtil.setContentView(this, R.layout.activity_register)

        screenComponent.inject(this)

        binding.let {
            it.lifecycleOwner = this
            it.viewModel = viewModel

            setupError(it.email, viewModel.emailError)
            setupError(it.name, viewModel.nameError)
            setupError(it.password, viewModel.passwordError)
            setupError(it.confirmPassword, viewModel.confirmPasswordError)
        }

        viewModel.let{
            it.incorrectPasswordConfirmation.observe(this,
                Observer { bool ->
                    if(bool) binding.confirmPassword.error = getString(R.string.password_must_match)
                }
            )

            it.existRegister.observe(this,
                Observer {bool->
                    if (bool) binding.email.error = getString(R.string.email_registered)

                }
            )

            it.successRegister.observe(this,
                Observer {bool->
                    if (bool){
                        Toast.makeText(this, getString(R.string.success_register), Toast.LENGTH_LONG).show()
                    }
                }
            )

            it.failureRegister.observe(this,
                Observer {bool->
                    if (bool){
                        Toast.makeText(this, getString(R.string.failed_to_register), Toast.LENGTH_LONG).show()
                    }
                }
            )
        }
    }
}