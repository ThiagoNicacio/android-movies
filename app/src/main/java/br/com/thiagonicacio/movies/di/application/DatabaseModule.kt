package br.com.thiagonicacio.movies.di.application

import android.app.Application
import androidx.room.Room
import br.com.thiagonicacio.data.storage.database.AppDatabase
import br.com.thiagonicacio.data.storage.database.MovieDao
import br.com.thiagonicacio.data.storage.database.UserDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {
    @Provides
    @Singleton
    fun provideRoomDatabase(application: Application): AppDatabase {
        return synchronized(this) {
            Room.databaseBuilder(
                application,
                AppDatabase::class.java,
                AppDatabase.DB_NAME
            ).build()
        }
    }

    @Provides
    @Singleton
    fun provideMovieDao(appDatabase: AppDatabase) : MovieDao {
        return appDatabase.movieDao
    }

    @Provides
    @Singleton
    fun provideUserDao(appDatabase: AppDatabase) : UserDao {
        return appDatabase.userDao
    }
}