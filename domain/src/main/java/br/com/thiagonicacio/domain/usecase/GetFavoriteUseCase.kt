package br.com.thiagonicacio.domain.usecase

import br.com.thiagonicacio.domain.GetFavoriteResult
import br.com.thiagonicacio.domain.repository.FavoriteRepository
import io.reactivex.Observable
import javax.inject.Inject

class GetFavoriteUseCase @Inject constructor(
    private val favoriteRepository: FavoriteRepository
) {

    suspend fun execute(id : Int) : Observable<GetFavoriteResult>? {
        return favoriteRepository.getFavorite(id)
            ?.toObservable()
            ?.map {
                GetFavoriteResult.Success(it) as GetFavoriteResult
            }
            ?.onErrorReturn { GetFavoriteResult.Failure(it) }
            ?.startWith( GetFavoriteResult.Loading )
    }
}