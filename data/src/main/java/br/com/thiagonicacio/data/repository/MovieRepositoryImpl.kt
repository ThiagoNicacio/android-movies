package br.com.thiagonicacio.data.repository

import br.com.thiagonicacio.data.api.MovieApi
import br.com.thiagonicacio.data.mapper.MovieMapper
import br.com.thiagonicacio.domain.model.Movie
import br.com.thiagonicacio.domain.repository.MovieRepository
import io.reactivex.Single
import javax.inject.Inject

class MovieRepositoryImpl @Inject constructor(
    private val movieApi: MovieApi,
    private val movieMapper: MovieMapper
) : MovieRepository{
    override fun getMovies(): Single<List<Movie>> {
        return movieApi.getMovies().map { movieMapper.map(it) }
    }
}