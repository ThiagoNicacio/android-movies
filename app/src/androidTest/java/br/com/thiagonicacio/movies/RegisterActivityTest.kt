package br.com.thiagonicacio.movies

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import br.com.thiagonicacio.movies.presentation.register.RegisterActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class RegisterActivityTest {

    @get:Rule
    val rule = ActivityTestRule(RegisterActivity::class.java)

    @Test
    fun shouldShowErrorOnMandatoryField_WhenEmailFieldIsEmpty(){
        onView(withId(R.id.register)).perform(click())
        onView(withId(R.id.email))
            .check(matches(isDisplayed()))
    }

    @Test
    fun shouldShowToast_WhenRegisterFailed(){
        onView(withId(R.id.email))
            .perform(typeText("teste3@teste.com"))
            .perform(closeSoftKeyboard())

        onView(withId(R.id.name))
            .perform(typeText("Teste"))
            .perform(closeSoftKeyboard())

        onView(withId(R.id.password))
            .perform(typeText("teste"))
            .perform(closeSoftKeyboard())

        onView(withId(R.id.confirmPassword))
            .perform(typeText("teste"))
            .perform(closeSoftKeyboard())

        onView(withId(R.id.register)).perform(click())
        onView(withId(R.id.email))
            .check(matches(isDisplayed()))
    }
}