package br.com.thiagonicacio.domain.usecase

import br.com.thiagonicacio.domain.repository.AuthRepository
import javax.inject.Inject

class LogoutUseCase @Inject constructor(private val authRepository: AuthRepository){

    fun execute() {
        return authRepository.logout()
    }
}