package br.com.thiagonicacio.movies.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.thiagonicacio.domain.model.Movie
import br.com.thiagonicacio.movies.databinding.ItemFavoriteBinding

class FavoriteAdapter(private var favorites: List<Movie>) :
    RecyclerView.Adapter<FavoriteAdapter.Holder>(), AdapterContract{

    lateinit var onItemClickedListener: (movie: Movie) -> Unit
    lateinit var onDisfavorClickedListener : (movie : Movie) -> Unit

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(
            ItemFavoriteBinding.inflate(LayoutInflater.from(parent.context), parent, false), onItemClickedListener, onDisfavorClickedListener)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(favorites[position])
    }

    class Holder(
        private val binding: ItemFavoriteBinding,
        private val onItemClickListener : ((item: Movie) -> Unit)?,
        private val onDisfavorClickedListener : ((item: Movie) -> Unit)?) :
        RecyclerView.ViewHolder(binding.root) {

        lateinit var favorite: Movie

        fun bind(favorite: Movie) {
            this.favorite = favorite
            binding.viewHolder = this
            binding.root.setOnClickListener { onItemClickListener?.invoke(favorite) }
            binding.favorite.setOnClickListener { onDisfavorClickedListener?.invoke(favorite) }
        }
    }

    override fun getItemCount(): Int {
        return favorites.count()
    }

    override fun replaceItems(items: List<*>) {
        this.favorites = items.filterIsInstance<Movie>() as ArrayList<Movie>
        notifyDataSetChanged()

    }
}