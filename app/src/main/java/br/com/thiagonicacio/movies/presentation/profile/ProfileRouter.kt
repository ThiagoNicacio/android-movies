package br.com.thiagonicacio.movies.presentation.profile

import android.app.Activity
import android.os.Bundle
import br.com.thiagonicacio.movies.presentation.BaseRouter
import br.com.thiagonicacio.movies.presentation.login.LoginActivity
import java.lang.ref.WeakReference

class ProfileRouter (activityRef: WeakReference<Activity>) : BaseRouter(activityRef){
    enum class Route {
        LOGIN
    }

    fun navigate(route: Route, bundle: Bundle = Bundle()) {
        when (route) {
            Route.LOGIN -> { showNextScreenClearTask(LoginActivity::class.java, bundle) }
        }
    }
}