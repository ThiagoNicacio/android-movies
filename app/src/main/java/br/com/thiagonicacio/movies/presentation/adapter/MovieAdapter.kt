package br.com.thiagonicacio.movies.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.thiagonicacio.domain.model.Movie
import br.com.thiagonicacio.movies.databinding.ItemMovieBinding

class MovieAdapter(private var movies: List<Movie>) :
    RecyclerView.Adapter<MovieAdapter.Holder>(), AdapterContract{

    lateinit var onItemClickedListener: (movie: Movie) -> Unit

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(
            ItemMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false), onItemClickedListener)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(movies[position])
    }

    class Holder(
        private val binding: ItemMovieBinding,
        private val onItemClickListener : ((item: Movie) -> Unit)?) :
        RecyclerView.ViewHolder(binding.root) {

        lateinit var movie: Movie

        fun bind(movie: Movie) {
            this.movie = movie
            binding.viewHolder = this
            binding.root.setOnClickListener { onItemClickListener?.invoke(movie) }
        }
    }

    override fun getItemCount(): Int {
        return movies.count()
    }

    override fun replaceItems(items: List<*>) {
        this.movies = items.filterIsInstance<Movie>() as ArrayList<Movie>
        notifyDataSetChanged()

    }
}