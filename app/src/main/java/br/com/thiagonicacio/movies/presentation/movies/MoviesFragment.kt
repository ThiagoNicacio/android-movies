package br.com.thiagonicacio.movies.presentation.movies

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.thiagonicacio.movies.R
import br.com.thiagonicacio.movies.databinding.MoviesFragmentBinding
import br.com.thiagonicacio.movies.presentation.BaseFragment
import br.com.thiagonicacio.movies.presentation.BaseViewModel
import br.com.thiagonicacio.movies.presentation.adapter.MovieAdapter
import javax.inject.Inject

class MoviesFragment : BaseFragment() {

    companion object {
        fun newInstance() = MoviesFragment()
    }

    @Inject
    lateinit var viewModel: MoviesViewModel

    override fun getBaseViewModel() = viewModel

    lateinit var binding: MoviesFragmentBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding : MoviesFragmentBinding = MoviesFragmentBinding.inflate(inflater,
            container, false)
        screenComponent.inject(this)

        binding.let {
            it.lifecycleOwner = requireActivity()
            it.viewModel = viewModel
            it.recyclerMovies.layoutManager = LinearLayoutManager(it.recyclerMovies.context)
        }

        viewModel.let {
            it.movies.observe(viewLifecycleOwner,
                Observer {list->
                    list?.let{movies ->
                        val adapter = MovieAdapter(movies)
                        adapter.onItemClickedListener = { viewModel.onClickItem(it) }
                        binding.recyclerMovies.adapter = adapter
                    }
                }
            )

            it.failureMovies.observe(viewLifecycleOwner,
                Observer {bool ->
                    if (bool) Toast.makeText(requireContext(),getString(R.string.failed_get_list_movies), Toast.LENGTH_LONG).show()
                }
            )
        }

        return binding.root
    }
}