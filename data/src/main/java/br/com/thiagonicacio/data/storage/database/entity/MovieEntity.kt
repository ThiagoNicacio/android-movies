package br.com.thiagonicacio.data.storage.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "movie_table")
data class MovieEntity(
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    val title: String,
    val sub_title: String,
    val duration: String,
    val synopsis: String,
    val thumb: String,
    val user: String
)