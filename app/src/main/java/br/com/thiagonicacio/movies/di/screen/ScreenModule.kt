package br.com.thiagonicacio.movies.di.screen

import br.com.thiagonicacio.movies.di.scope.PerScreen
import br.com.thiagonicacio.movies.presentation.BaseActivity
import br.com.thiagonicacio.movies.presentation.favorites.FavoriteRouter
import br.com.thiagonicacio.movies.presentation.login.LoginRouter
import br.com.thiagonicacio.movies.presentation.moviedetails.MovieDetailsRouter
import br.com.thiagonicacio.movies.presentation.movies.MoviesRouter
import br.com.thiagonicacio.movies.presentation.profile.ProfileRouter
import br.com.thiagonicacio.movies.presentation.register.RegisterRouter
import dagger.Module
import dagger.Provides
import java.lang.ref.WeakReference

@Module
class ScreenModule(private val activity: BaseActivity) {
    @PerScreen
    @Provides
    fun providesActivity(): BaseActivity {
        return activity
    }

    @PerScreen
    @Provides
    fun providesMoviesRouter(): MoviesRouter{
        return MoviesRouter(WeakReference(activity))
    }

    @PerScreen
    @Provides
    fun providesMovieDetailsRouter(): MovieDetailsRouter{
        return MovieDetailsRouter(WeakReference(activity))
    }

    @PerScreen
    @Provides
    fun providesFavoriteRouter(): FavoriteRouter{
        return FavoriteRouter(WeakReference(activity))
    }

    @PerScreen
    @Provides
    fun providesLoginRouter(): LoginRouter{
        return LoginRouter(WeakReference(activity))
    }

    @PerScreen
    @Provides
    fun providesRegisterRouter(): RegisterRouter{
        return RegisterRouter(WeakReference(activity))
    }

    @PerScreen
    @Provides
    fun providesProfileRouter(): ProfileRouter{
        return ProfileRouter(WeakReference(activity))
    }
}