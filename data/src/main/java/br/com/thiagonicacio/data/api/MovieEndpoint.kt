package br.com.thiagonicacio.data.api

import br.com.thiagonicacio.data.response.MovieResponse
import io.reactivex.Single
import retrofit2.http.GET

interface MovieEndpoint {
    @GET("movies")
    fun getMovie() : Single<List<MovieResponse>>
}