package br.com.thiagonicacio.domain.model

import java.io.Serializable

data class Movie(
    val id : Int,
    val title: String,
    val subTitle: String,
    val duration : String,
    val synopsis : String,
    val thumb : String
) : Serializable