package br.com.thiagonicacio.domain.repository

import br.com.thiagonicacio.domain.model.Movie
import io.reactivex.Single

interface MovieRepository {
    fun getMovies() : Single<List<Movie>>
}