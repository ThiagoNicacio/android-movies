package br.com.thiagonicacio.movies.databinding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

class ImageBinding {
    companion object{
        @BindingAdapter("loadImage")
        @JvmStatic
        fun loadImage(view: ImageView, imageUrl: String?) {
            if(imageUrl.isNullOrBlank())
                return
            Picasso.get()
                .load(imageUrl)
                .resize(300, 0)
                .into(view)
        }
    }
}