package br.com.thiagonicacio.movies.presentation.movies

import android.app.Activity
import android.os.Bundle
import br.com.thiagonicacio.movies.presentation.BaseRouter
import br.com.thiagonicacio.movies.presentation.moviedetails.MovieDetailsActivity
import java.lang.ref.WeakReference

class MoviesRouter (activityRef: WeakReference<Activity>) : BaseRouter(activityRef){
    enum class Route {
        MOVIE_DETAILS
    }

    fun navigate(route: Route, bundle: Bundle = Bundle()) {
        when (route) {
            Route.MOVIE_DETAILS -> { showNextScreen(MovieDetailsActivity::class.java, bundle) }
        }
    }
}