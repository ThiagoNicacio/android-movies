package br.com.thiagonicacio.movies.presentation.main

import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import br.com.thiagonicacio.movies.R
import br.com.thiagonicacio.movies.R.layout
import br.com.thiagonicacio.movies.databinding.ActivityMainBinding
import br.com.thiagonicacio.movies.presentation.BaseActivity
import br.com.thiagonicacio.movies.presentation.favorites.FavoritesFragment
import br.com.thiagonicacio.movies.presentation.movies.MoviesFragment
import br.com.thiagonicacio.movies.presentation.profile.ProfileFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity(), BottomNavigationView.OnNavigationItemSelectedListener {
    @Inject
    lateinit var viewModel: MainViewModel

    private val movies = MoviesFragment.newInstance()
    private val favorites = FavoritesFragment.newInstance()
    private val profile = ProfileFragment.newInstance()

    override fun getBaseViewModel() = viewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this, layout.activity_main)
        screenComponent.inject(this)

        binding.let {
            it.viewModel = viewModel

        }
        bottom_navigation.setOnNavigationItemSelectedListener(this)

        openFragment(movies)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.movie ->{
                openFragment(movies)
            }
            R.id.favorite ->{
                openFragment(favorites)
            }
            R.id.profile -> {
                openFragment(profile)
            }
        }
        return true
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.commit()
    }
}