package br.com.thiagonicacio.movies.di.application

import br.com.thiagonicacio.data.api.MovieApi
import br.com.thiagonicacio.data.storage.database.AppDatabase
import br.com.thiagonicacio.data.mapper.MovieMapper
import br.com.thiagonicacio.data.mapper.UserMapper
import br.com.thiagonicacio.data.repository.AuthRepositoryImpl
import br.com.thiagonicacio.data.repository.FavoriteRepositoryImpl
import br.com.thiagonicacio.data.repository.MovieRepositoryImpl
import br.com.thiagonicacio.data.repository.UserRepositoryImpl
import br.com.thiagonicacio.data.storage.SessionManager
import br.com.thiagonicacio.domain.repository.AuthRepository
import br.com.thiagonicacio.domain.repository.FavoriteRepository
import br.com.thiagonicacio.domain.repository.MovieRepository
import br.com.thiagonicacio.domain.repository.UserRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideMovieRepository(movieApi: MovieApi, movieMapper: MovieMapper) : MovieRepository{
        return MovieRepositoryImpl(movieApi, movieMapper)
    }

    @Provides
    @Singleton
    fun provideFavoriteRepository(appDatabase: AppDatabase, movieMapper: MovieMapper) : FavoriteRepository {
        return FavoriteRepositoryImpl(movieMapper, appDatabase)
    }

    @Provides
    @Singleton
    fun provideAuthRepository(sessionManager: SessionManager) : AuthRepository{
        return AuthRepositoryImpl(sessionManager)
    }

    @Provides
    @Singleton
    fun provideUserRepository(appDatabase: AppDatabase, userMapper: UserMapper) : UserRepository {
        return UserRepositoryImpl(userMapper, appDatabase)
    }
}