package br.com.thiagonicacio.movies.presentation.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.thiagonicacio.movies.databinding.ProfileFragmentBinding
import br.com.thiagonicacio.movies.presentation.BaseFragment
import javax.inject.Inject

class ProfileFragment : BaseFragment() {

    companion object {
        fun newInstance() = ProfileFragment()
    }

    @Inject
    lateinit var viewModel: ProfileViewModel

    override fun getBaseViewModel() = viewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding : ProfileFragmentBinding = ProfileFragmentBinding.inflate(inflater, container, false)
        screenComponent.inject(this)

        binding.let {
            it.lifecycleOwner = requireActivity()
            it.viewModel = viewModel
        }

        return binding.root
    }
}