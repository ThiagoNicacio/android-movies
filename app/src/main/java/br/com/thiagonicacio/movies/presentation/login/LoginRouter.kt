package br.com.thiagonicacio.movies.presentation.login

import android.app.Activity
import android.os.Bundle
import br.com.thiagonicacio.movies.presentation.BaseRouter
import br.com.thiagonicacio.movies.presentation.main.MainActivity
import br.com.thiagonicacio.movies.presentation.register.RegisterActivity
import java.lang.ref.WeakReference

class LoginRouter (activityRef: WeakReference<Activity>) : BaseRouter(activityRef){
    enum class Route {
        MAIN,
        REGISTER
    }

    fun navigate(route: Route, bundle: Bundle = Bundle()) {
        when (route) {
            Route.MAIN -> { showNextScreenClearTask(MainActivity::class.java, bundle) }
            Route.REGISTER -> { showNextScreen(RegisterActivity::class.java, bundle) }
        }
    }
}