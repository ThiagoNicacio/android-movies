package br.com.thiagonicacio.data.repository

import br.com.thiagonicacio.data.storage.database.AppDatabase
import br.com.thiagonicacio.data.storage.database.entity.UserEntity
import br.com.thiagonicacio.data.mapper.UserMapper
import br.com.thiagonicacio.domain.model.User
import br.com.thiagonicacio.domain.repository.UserRepository
import io.reactivex.Single
import javax.inject.Inject

class UserRepositoryImpl @Inject constructor(
    private val userMapper: UserMapper,
    private val appDatabase: AppDatabase): UserRepository {

    override suspend fun insertUser(email: String, name : String, password : String): Single<Boolean> {
        return try {
            appDatabase.userDao.insertUser(UserEntity(
                email, name, password
            ))

            Single.just(true)
        }catch (e : Exception){
            Single.just(false)
        }
    }

    override suspend fun getUser(email: String): Single<User?>? {
        val us = appDatabase.userDao.getUser(email) ?: return null
        return Single.just(userMapper.map(us))
    }

    override suspend fun login(email: String, password: String): Single<Boolean> {
        return try {
            val us = appDatabase.userDao.login(email, password)
            if (us == null)
                Single.just(false)
            else
                Single.just(true)
        }catch (e : Exception){
            Single.just(false)
        }
    }
}