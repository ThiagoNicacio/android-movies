package br.com.thiagonicacio.data.api

import br.com.thiagonicacio.data.response.MovieResponse
import io.reactivex.Single
import javax.inject.Inject

class MovieApi @Inject constructor(private val movieEndpoint: MovieEndpoint){
    fun getMovies() : Single<List<MovieResponse>>{
        return movieEndpoint.getMovie()
    }
}