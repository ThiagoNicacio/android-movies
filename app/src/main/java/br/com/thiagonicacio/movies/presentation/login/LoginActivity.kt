package br.com.thiagonicacio.movies.presentation.login

import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import br.com.thiagonicacio.movies.R
import br.com.thiagonicacio.movies.databinding.ActivityLoginBinding
import br.com.thiagonicacio.movies.presentation.BaseActivity
import javax.inject.Inject

class LoginActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: LoginViewModel
    override fun getBaseViewModel() = viewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding : ActivityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login)

        screenComponent.inject(this)

        binding.let {
            it.lifecycleOwner = this
            it.viewModel = viewModel

            setupError(it.email, viewModel.emailError)
            setupError(it.password, viewModel.passwordError)
        }

        viewModel.let {
            it.invalidCredentials.observe(this,
                Observer { bool ->
                    if (bool){
                        Toast.makeText(this, getString(R.string.invalid_credentials), Toast.LENGTH_LONG).show()
                    }
                }
            )

            it.failureLogin.observe(this,
                Observer {bool ->
                    if (bool){
                        Toast.makeText(this, getString(R.string.failed_to_login), Toast.LENGTH_LONG).show()
                    }
                }
            )

            it.failureGetUser.observe(this,
                Observer {bool ->
                    if (bool){
                        Toast.makeText(this, getString(R.string.failed_get_user), Toast.LENGTH_LONG).show()
                    }
                }
            )
        }
    }
}