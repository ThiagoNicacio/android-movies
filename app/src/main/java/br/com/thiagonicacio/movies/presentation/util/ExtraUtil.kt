package br.com.thiagonicacio.movies.presentation.util

/*
    Classe para centralizar as keys dos bundles usados em intents
 */

class ExtraUtil {
    companion object {
        const val EXTRA_MOVIE = "movie"
    }
}