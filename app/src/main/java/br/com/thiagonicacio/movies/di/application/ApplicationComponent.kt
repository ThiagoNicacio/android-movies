package br.com.thiagonicacio.movies.di.application

import br.com.thiagonicacio.movies.BaseApplication
import br.com.thiagonicacio.movies.di.screen.ScreenComponent
import br.com.thiagonicacio.movies.di.screen.ScreenModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, RepositoryModule::class, EndpointModule::class, DatabaseModule::class])
interface ApplicationComponent {

    fun inject(activity: BaseApplication)

    fun plus(screenModule: ScreenModule): ScreenComponent
}